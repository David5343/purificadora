/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package istmo.plus.vistas.forms;

import java.beans.PropertyVetoException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lenovo
 */
public class ListarEmpleado extends javax.swing.JInternalFrame {

    /**
     * Creates new form ListarEmpleado
     */
    public ListarEmpleado(javax.swing.JDesktopPane panePadre) {
        initComponents();
       this.jDesktopPane =   panePadre;
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        Table_Empleados = new javax.swing.JTable();
        ButtonBuscar = new javax.swing.JButton();
        ComboBoxTipoBusca = new javax.swing.JComboBox();
        TextBusqueda = new javax.swing.JTextField();
        LabelBusquedaPor = new javax.swing.JLabel();
        Button_NuevoE = new javax.swing.JButton();

        setClosable(true);
        setTitle("LISTA DE EMPLEADOS");

        Table_Empleados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Codigo", "Nombre", "Domicilio", "Telefono", "Tipo", "Fecha de Alta"
            }
        ));
        jScrollPane1.setViewportView(Table_Empleados);

        ButtonBuscar.setText("Buscar");

        ComboBoxTipoBusca.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Codigo", "Nombre" }));

        LabelBusquedaPor.setText("Busqueda Por:");

        Button_NuevoE.setText("Nuevo Empleado");
        Button_NuevoE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Button_NuevoEActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(TextBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(LabelBusquedaPor)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(ComboBoxTipoBusca, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(ButtonBuscar)
                        .addGap(33, 33, 33)
                        .addComponent(Button_NuevoE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 510, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(60, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TextBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ComboBoxTipoBusca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ButtonBuscar)
                    .addComponent(LabelBusquedaPor)
                    .addComponent(Button_NuevoE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(145, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Button_NuevoEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Button_NuevoEActionPerformed
        // TODO add your handling code here:
        
        istmo.plus.vistas.forms.AgregarEmpleado nuevoE = new istmo.plus.vistas.forms.AgregarEmpleado();
        nuevoE.setVisible(true);
        this.jDesktopPane.add(nuevoE);
                try {
            nuevoE.setSelected(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(ListarEmpleado.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_Button_NuevoEActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ButtonBuscar;
    private javax.swing.JButton Button_NuevoE;
    private javax.swing.JComboBox ComboBoxTipoBusca;
    private javax.swing.JLabel LabelBusquedaPor;
    private javax.swing.JTable Table_Empleados;
    private javax.swing.JTextField TextBusqueda;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
    private javax.swing.JDesktopPane jDesktopPane;
}

